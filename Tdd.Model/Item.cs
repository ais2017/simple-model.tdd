using System;
namespace Tdd.Model
{
    public class Item
    {
        public Item(double volume)
        {
            if (volume <= 0)
            {
                throw new ArgumentException();
            }

            this.Volume = volume;
        }

        public double Volume { get; }
    }
}