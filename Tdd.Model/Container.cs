﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Tdd.Model
{
    public class Container
    {
        public Container(double volume)
        {
            if (volume <= 0)
            {
                throw new ArgumentException();
            }
            this.Volume = volume;
        }

        public void AddItem(Item item)
        {
            if (this.items.Sum(i => i.Volume) + item.Volume > this.Volume)
            {
                throw new ArgumentException();
            }
            this.items.Add(item);
        }

        public double Volume { get; }
        public IReadOnlyList<Item> Items { get { return this.items; } }

        private List<Item> items = new List<Item>();
    }
}
