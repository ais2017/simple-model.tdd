using NUnit.Framework;
using Tdd.Model;
using System;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void VolumeSetup()
        {
            var container = new Container(100.0);

            Assert.That(container.Volume, Is.EqualTo(100.0).Within(4).Ulps);
        }

        [Test]
        public void ElementCouldBeAdded()
        {
            var container = new Container(100.0);
            var item = new Item(10.0);
            
            container.AddItem(item);

            Assert.That(container.Items, Is.EqualTo(new[]{ item }));
        }

        [Test]
        public void ContainerVolumeMustGeGreaterThanZero()
        {   
            Assert.That(
                () => new Container(-10.0),
                Throws.ArgumentException
            );
        }

        [Test]
        public void ItemVolumeMustGeGreaterThanZero()
        {   
            Assert.That(
                () => new Item(-10.0),
                Throws.ArgumentException
            );
        }

        [Test]
        public void ItemVolumeIsSetOnConstruction()
        {
            var item = new Item(100.3);

            Assert.That(item.Volume, Is.EqualTo(100.3).Within(1).Ulps);
        }

        [Test]
        public void ContainerMustHaveEnoughtVolumeForItem()
        {
            var item = new Item(10);
            var container = new Container(5);

            Assert.That(
                () => container.AddItem(item),
                Throws.ArgumentException
            );
        }

        [Test]
        public void ContainerMustCalculateFreeSpaceBasedOnItsItems()
        {
            var item1 = new Item(10);
            var item2 = new Item(10);
            var container = new Container(15.0);
            container.AddItem(item1);

            Assert.That(
                () => container.AddItem(item2),
                Throws.ArgumentException
            );
        }
    }
}