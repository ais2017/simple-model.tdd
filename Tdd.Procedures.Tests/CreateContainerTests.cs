
using System.Collections.Generic;
using System;
using NUnit.Framework;
using Tdd.Procedures;
using Tdd.Model;

namespace Tests
{
    public class CreateContainerTests
    {
        class TestContainerGateway : IContainerGateway
        {
            public Container container;
            public int Create(Container container)
            {
                this.container = container;
                return 100500;
            }

            public Container Get(int id)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<Tuple<int, Container>> GetAllContainers()
            {
                throw new NotImplementedException();
            }

            public int GetKey(Container container)
            {
                throw new NotImplementedException();
            }
        }

        TestContainerGateway gateway;
        CreateContainer procedure;

        [SetUp]
        public void Init()
        {
            this.gateway = new TestContainerGateway();
            this.procedure = new CreateContainer(gateway);
        }

        [Test]
        public void ContainerPassedIntoGateway()
        {
            var id = procedure.Execute(100.5);

            Assert.That(gateway.container, Is.Not.Null);
        }

        [Test]
        public void VolumeMatches()
        {
            var id = this.procedure.Execute(100.5);

            Assert.That(this.gateway.container.Volume, Is.EqualTo(100.5).Within(1).Ulps);
        }

        [Test]
        public void IdReturnedFromGatewayReturnedFromProcedure()
        {
            var id = procedure.Execute(100.5);

            Assert.That(id, Is.EqualTo(100500));
        }
    }
}