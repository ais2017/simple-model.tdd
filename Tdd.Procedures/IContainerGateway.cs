using System.Collections.Generic;
using Tdd.Model;
using System;
namespace Tdd.Procedures
{
    public interface IContainerGateway
    {
        int Create(Container container);
        Container Get(int id);
        IEnumerable<Tuple<int, Container>> GetAllContainers();
        int GetKey(Container container);
    }
}