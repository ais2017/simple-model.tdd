﻿using System;
using Tdd.Model;

namespace Tdd.Procedures
{
    public class CreateContainer
    {
        IContainerGateway db;
        public CreateContainer(IContainerGateway db)
        {
            this.db = db;
        }
        public int Execute(double volume)
        {
            return this.db.Create(new Container(volume));
        }
    }
}
